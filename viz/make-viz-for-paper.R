source(here::here("R/helper_functions.R"))
library(ggplot2)
library(dplyr)
library(purrr)
library(tibble)
library(pammtools)  # for geom_stepribbon


# Seed number: 1
# Product price: $12

set.seed(1)
iv <- generate_individual_values(price = 12, quantity = 6)
mv <- compute_market_values(iv)

# Tim's demand curve

.data <- tidyr::replace_na(data = iv[[1]], replace = list("Price greater than" = 0))

.data %>%
  ggplot(aes(x = Quantity, y = `Price greater than`)) +
  geom_step(size = 1) +
  geom_point(size = 2) +
  labs(x = "Quantity demanded", y = "Price") +
  scale_x_continuous(breaks = .data$Quantity, labels = .data$Quantity, expand = c(0, 0)) +
  scale_y_continuous(breaks = .data$`Price greater than`, labels = .data$`Price greater than`) +
  expand_limits(x = c(0, 6.5), y = 0) +
  theme_bw() +
  theme(text = element_text(size = 7))

ggsave(here::here("viz/individual-demand-curve.png"))

# market demand curve
.data <- tidyr::replace_na(data = mv, replace = list("Price greater than" = 0))

.data %>%
  ggplot(aes(x = Quantity, y = `Price greater than`)) +
  geom_step(size = 1) +
  geom_point(size = 2) +
  labs(x = "Quantity demanded", y = "Price") +
  scale_x_continuous(breaks = .data$Quantity, labels = .data$Quantity, expand = c(0, 0)) +
  scale_y_continuous(breaks = .data$`Price greater than`, labels = .data$`Price greater than`) +
  expand_limits(x = c(0, 18.5), y = 0) +
  theme_bw() +
  theme(text = element_text(size = 7))

ggsave(here::here("viz/market-demand-curve.png"))

# Tim's demand curve with consumer surplus

.data <- tidyr::replace_na(data = iv[[1]], replace = list("Price greater than" = 0))
price <- 12

.data %>%
  ggplot(aes(x = Quantity, y = `Price greater than`)) +
  geom_step(size = 1) +
  geom_point(size = 2) +
  labs(x = "Quantity demanded", y = "Price") +
  scale_x_continuous(breaks = .data$Quantity, labels = .data$Quantity, expand = c(0, 0)) +
  scale_y_continuous(breaks = c(.data$`Price greater than`, price), labels = c(.data$`Price greater than`, price)) +
  expand_limits(x = c(0, 6.5), y = 0) +
  theme_bw() +
  geom_stepribbon(aes(ymin = pmin(price, .data$`Price greater than`), ymax = .data$`Price greater than`), fill = "blue", alpha = 0.2) +
  geom_hline(yintercept = 12, size = 1, color = "red") +
  theme(text = element_text(size = 7))

ggsave(here::here("viz/individual-demand-curve-consumer-surplus.png"))

# Market demand curve with consumer surplus

.data <- tidyr::replace_na(data = mv, replace = list("Price greater than" = 0))

.data %>%
  ggplot(aes(x = Quantity, y = `Price greater than`)) +
  geom_step(size = 1) +
  geom_point(size = 2) +
  labs(x = "Quantity demanded", y = "Price") +
  scale_x_continuous(breaks = .data$Quantity, labels = .data$Quantity, expand = c(0, 0)) +
  scale_y_continuous(breaks = c(.data$`Price greater than`, price), labels = c(.data$`Price greater than`, price)) +
  expand_limits(x = c(0, 18.5), y = 0) +
  expand_limits(x = c(0, 6.5), y = 0) +
  theme_bw() +
  geom_stepribbon(aes(ymin = pmin(price, .data$`Price greater than`), ymax = .data$`Price greater than`), fill = "blue", alpha = 0.2) +
  geom_hline(yintercept = 12, size = 1, color = "red") +
  theme(text = element_text(size = 7))

ggsave(here::here("viz/market-demand-curve-consumer-surplus.png"))
