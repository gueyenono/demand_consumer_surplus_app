##################################################
# ~*~ SHINY MODULE FOR PLOTTING DEMAND CURVE ~*~ #
##################################################


plot_module_ui <- function(id){
  
  ns <- NS(id)
  
  plotOutput(outputId = ns("demand_plot"))
  
}

plot_module <- function(input, output, session, .data, price, all_correct, consumer_surplus = FALSE){
  
  output$demand_plot <- renderPlot({
    
    .data <- tidyr::replace_na(data = .data, replace = list("Price greater than" = 0))
    
    if(all_correct & !consumer_surplus){
      
      p <- .data %>%
        ggplot(aes(x = Quantity, y = `Price greater than`)) +
        geom_step(size = 1) +
        geom_point(size = 2) +
        labs(x = "Quantity demanded", y = "Price") +
        scale_x_continuous(breaks = .data$Quantity, labels = .data$Quantity, expand = c(0, 0)) +
        scale_y_continuous(breaks = .data$`Price greater than`, labels = .data$`Price greater than`) +
        expand_limits(x = c(0, max(.data$`Quantity demanded`)+0.5), y = 0) +
        theme_bw()
      
      
    } else if(all_correct & consumer_surplus){
      
      p <- .data %>%
        ggplot(aes(x = Quantity, y = `Price greater than`)) +
        geom_step(size = 1) +
        geom_point(size = 2) +
        labs(x = "Quantity demanded", y = "Price") +
        scale_x_continuous(breaks = .data$Quantity, labels = .data$Quantity, expand = c(0, 0)) +
        scale_y_continuous(breaks = c(.data$`Price greater than`, price), labels = c(.data$`Price greater than`, price)) +
        expand_limits(x = c(0, max(.data$`Quantity demanded`)+0.5), y = 0) +
        theme_bw() +
        geom_stepribbon(aes(ymin = pmin(price, .data$`Price greater than`), ymax = .data$`Price greater than`), fill = "blue", alpha = 0.2) +
        geom_hline(yintercept = price, size = 1, color = "red") 
      
      
    } else {
      
      p <- NULL
      
    }
    
    p
  })
  
  return(p)

}